```
input {
  file {
    type => "json"
    path => "/usr/share/logstash/pipeline/data/data.json"
    start_position => "beginning"
    sincedb_path => "NUL"
  }
}

filter {
  json {
    source => "message"
  }
  mutate {
    add_field => {
      "test_value_number" => "%{[test_value]}"
    }
  }
  prune {
    whitelist_values => [ "test_value_number", "[0-9\+\-\,\.]+" ]
  }
  mutate {
    convert => { "test_value_number" => "float" }
  }
}

output {
  stdout {}
}
```
